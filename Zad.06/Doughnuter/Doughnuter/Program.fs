﻿// Learn more about F# at http://fsharp.org

open System
open System.Diagnostics
open System.IO
open System.Globalization
open System.Text

open Doughnut
open PostScript



type VerboseLevel =
    | Quiet
    | Normal
    | Loud


type Args =
    { verbosity : VerboseLevel
    ; sourcePath : string
    }


type ProgramArgs =
    | ShowHelp
    | Execute of Args


type Row =
    { index : int
    ; str : string
    }


type RawItem =
    { row : Row
    ; name : string
    ; value : string
    }



let appHeaderLines =
    [ "#############################################################"
    ; "###    Doughnuter - PostScript diagram generator in F#    ###"
    ; "###                    Kraków 2018,   Dominik Janiec      ###"
    ; "#############################################################"
    ]

let appHelpLines =
    [ ""
    ; "Execute like:"
    ; "$ doughnuter --verbose --verbose -- sample.csv"
    ; ""
    ]



let joinLines lines =
    String.concat Environment.NewLine lines

let verbosityLike level args =
    match (level, args.verbosity) with
    | (Loud, Loud) -> true
    | (Normal, Loud) -> true
    | (Normal, Normal) -> true
    | _ -> false

let failed (row: Row) (message: string) =
    sprintf "%s | Row: %d. '%s'" message (row.index + 1) row.str
        |> failwith



let parseArgs argv =
    let is key arg =
        String.Equals(sprintf "--%s" key, arg, StringComparison.InvariantCultureIgnoreCase)

    let asExecute args =
        let countVerbose c arg =
            match is "verbose" arg with
            | true -> c + 1
            | _ -> c

        let verbosityLevel =
            match Array.fold countVerbose 0 args with
            | 0 -> Quiet
            | 1 -> Normal
            | _ -> Loud

        { verbosity = verbosityLevel
        ; sourcePath = Array.last args
        } |> Execute

    match Array.tryHead argv with
    | None -> ShowHelp
    | Some arg ->
        match is "help" arg with
        | true -> ShowHelp
        | false -> asExecute argv



let loadRawItems file =
    let toRow i line =
        { index = i; str = line }

    let notEmpty row =
        String.IsNullOrWhiteSpace(row.str) = false

    let split (row: Row) =
        let idx = row.str.LastIndexOf(";")
        if idx = -1 then
            failed row "Could not find any ';' (splitter char) in a row."

        let name = row.str.Substring(0, idx).Trim()
        if String.IsNullOrWhiteSpace(name) then
            failed row "Name (before last ';') is missing in a row."

        let value = row.str.Substring(idx + 1).Trim()
        if String.IsNullOrWhiteSpace(name) then
            failed row "Value (after last ';') is missing in a row."

        { row = row
        ; name = name
        ; value = value
        }

    File.ReadAllLines(file, Encoding.UTF8)
        |> Array.toList
        |> List.mapi toRow
        |> List.where notEmpty
        |> List.map split



let provideModel rawItems =
    let extract (name: string) =
        let trimmed = name.Trim()
        match trimmed.StartsWith('"') with
        | false -> trimmed
        | _ -> trimmed.Substring(1, trimmed.Length - 2).Trim()

    let parse (value: string) =
        Decimal.Parse(value, CultureInfo.InvariantCulture)

    let makeItem raw =
        try
            { name = extract raw.name
            ; value = parse raw.value
            }
        with
        | :? FormatException ->
            sprintf "Could not parse '%s' as Number for: '%s'." raw.value raw.name
                |> failed raw.row

    List.map makeItem rawItems
        |> Doughnut.fromList



let quotePostScript (lines: string list) =
    let padding =
        List.length lines
            |> fun n -> Math.Log10(float n)
            |> fun value -> (int value) + 1

    let numerate (order: int) =
        (order + 1).ToString(CultureInfo.InvariantCulture).PadLeft(padding)

    let decorate n line =
        sprintf "\t%s. > %s" (numerate n) line

    Seq.mapi decorate lines
        |> joinLines



let savePostScript scriptLines =
    let outputDir = "donuts"
    if Directory.Exists(outputDir) = false then
        Directory.CreateDirectory(outputDir)
            |> ignore

    let resultFile =
        (DateTime.Now.ToString("yyyyMMdd.HHmmss.fff"))
            |> sprintf "%s.ps"

    let fullPath =
        Path.Combine(Directory.GetCurrentDirectory(), outputDir, resultFile)

    let script = joinLines scriptLines
    File.WriteAllText(fullPath, script)
        |> ignore

    fullPath



let executeWith (args: Args) =
    let source = args.sourcePath
    if args |> verbosityLike Loud then
        printfn "\n  * Source path: '%s'" source

    let rawItems = loadRawItems source
    if args |> verbosityLike Loud then
        printfn "\n  * Loaded raw items:\n%A" rawItems

    let model = provideModel rawItems
    if args |> verbosityLike Normal then
        printfn "\n  * Calculated Model:\n%s" (prettified model)

    let script = postScriptOf appHeaderLines (PageA4, Portrait) model
    if args |> verbosityLike Loud then
        printfn "\n  * PostScript file:\n%s" (quotePostScript script)

    let psFilePath = savePostScript script
    if args |> verbosityLike Normal then
        printfn "\n  * PostScript saved under path:\n'%s'" psFilePath

    if args |> verbosityLike Loud then
        printfn "\n  * Executing `gsview` with generated script file..."

    Process.Start("gsview.exe", psFilePath)
        |> ignore



[<EntryPoint>]
let main argv =
    let printLines lines =
        lines |> Seq.iter (printfn "%s")

    printLines appHeaderLines

    match parseArgs argv with
    | Execute args -> executeWith args
    | ShowHelp -> printLines appHeaderLines

    0 // return an integer exit code
