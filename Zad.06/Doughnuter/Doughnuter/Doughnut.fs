﻿module Doughnut

open System



type Item =
    { name : string
    ; value : decimal
    }

type Doughnut =
    { title : string
    ; total : decimal
    ; items : Item list
    }



type Color =
    | Palette of decimal * decimal * decimal
    | BlackWhite of decimal

type ColorType =
    | ColorPalette
    | ColorBW



type Rad = decimal

type Angle =
    { offset : Rad
    ; value : Rad
    }



let fullAngle : Rad =
    2M * (decimal Math.PI)

let toDeg rad =
    (rad * 180M) / (decimal Math.PI)

let sizeOf donut =
    List.length donut.items

let private indexOf donut (item: Item) =
    List.findIndex item.Equals donut.items

let private itemValue (item: Item) =
    item.value



let prettified (donut: Doughnut) =
    // TODO: Make display format more friendly:
    sprintf "%A" donut



let fromList items =
    if items |> List.isEmpty then
        failwith "There is no Items to create Doughtnut"

    { title = "Sample - Dominik Janiec, 2018"
    ; total = List.sumBy itemValue items
    ; items = items
    }



let colorFor colorType donut item =
    let index =
        indexOf donut item

    let asPalette() =
        let knownColors =
            [ (255, 179, 0)
            ; (128, 62, 117)
            ; (255, 104, 0)
            ; (166, 189, 215)
            ; (193, 0, 32)
            ; (206, 162, 98)
            ; (129, 112, 102)
            ; (0, 125, 52)
            ; (246, 118, 142)
            ; (0, 83, 138)
            ; (255, 122, 92)
            ; (83, 55, 122)
            ; (255, 142, 0)
            ; (179, 40, 81)
            ; (244, 200, 0)
            ; (127, 24, 13)
            ; (147, 170, 0)
            ; (89, 51, 21)
            ; (241, 58, 19)
            ; (35, 44, 22)
            ]

        let maxIndex = knownColors.Length - 1
        let (r, g, b) = knownColors.[index % maxIndex]

        ( (decimal r) / 256M
        , (decimal g) / 256M
        , (decimal b) / 256M
        ) |> Palette

    let asBlackWhite() =
        let margin = 0.16M
        let band = (1M - (2M * margin))
        let step = band / (sizeOf donut |> decimal)
        let idx = index |> decimal

        (step * idx) + margin
            |> BlackWhite

    match colorType with
    | ColorPalette -> asPalette()
    | ColorBW -> asBlackWhite()



let angleFor donut (item: Item) =
    let untilItemSum =
        donut.items
            |> List.takeWhile ((<>) item)
            |> List.sumBy itemValue

    let offsetPart = untilItemSum / donut.total
    let valuePart = item.value / donut.total

    { offset = fullAngle * offsetPart
    ; value = fullAngle * valuePart
    }
