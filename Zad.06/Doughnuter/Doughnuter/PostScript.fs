﻿module PostScript

open Doughnut
open System.Globalization
open System



type PageOrientation =
    | Portrait
    | Landscape

type PageSize =
    | PageA4

type Page = PageSize * PageOrientation



type L = decimal

type Coord = { x : L ; y : L }



let private toStr (a: L) =
    Math.Round((decimal a), 2)
        .ToString(CultureInfo.InvariantCulture)

let private comment line =
    sprintf "%% %s" line

let private cmdSetBlack =
    "0 setgray"

let private setColorFor model item =
    match colorFor ColorPalette model item with
    | Palette (r, g, b) ->
        sprintf "%s %s %s setrgbcolor" (toStr r) (toStr g) (toStr b)
    | BlackWhite brightness ->
        sprintf "%s setgray" (toStr brightness)


let private percentage model (item: Item) =
    let cent = (item.value * 100M) / model.total
    Math.Round(cent, 3).ToString(CultureInfo.CurrentUICulture)
        |> sprintf "%s%%"


let private pageMeasure (pageConf: Page) =
    let max =
        match fst pageConf with
        | PageA4 -> { x = 595M ; y = 842M }

    match snd pageConf with
    | Landscape -> { x = max.y ; y = max.x }
    | Portrait -> max


let private arcPath (center, radius) angle =
    let x = toStr center.x
    let y = toStr center.y
    let r = toStr radius
    let start = toStr (angle.offset |> toDeg)
    let stop = toStr (angle.offset + angle.value |> toDeg)

    [ "newpath"
    ; sprintf "%s %s %s %s %s arc" x y r start stop
    ; sprintf "%s %s lineto" x y
    ; "closepath"
    ]


let private boxPath (low, high) =
    let lx = toStr low.x
    let ly = toStr low.y
    let hx = toStr high.x
    let hy = toStr high.y

    [ "newpath"
    ; sprintf "%s %s moveto" lx ly
    ; sprintf "%s %s lineto" hx ly
    ; sprintf "%s %s lineto" hx hy
    ; sprintf "%s %s lineto" lx hy
    ; "closepath"
    ]


let private debugBox name border =
#if DEBUG
    seq {
        yield ""
        yield sprintf "Debug box: '%s'" name |> comment
        yield! boxPath border
        yield "2 setlinewidth"
        yield cmdSetBlack
        yield "stroke"
    } |> List.ofSeq
#else
    List.empty
#endif


let private header desc pageConf model =
    let size = fst pageConf
    let orientation = snd pageConf
    let measure = pageMeasure pageConf

    let description =
        desc |> List.map comment

    let assumptions =
        [ sprintf "  * Height: %s" (toStr measure.x)
        ; sprintf "  * Width: %s" (toStr measure.y)
        ; sprintf "  * %A, %A" size orientation
        ] |> List.map comment

    let modelLines =
        (prettified model).Split("\n")
            |> Array.toList
            |> List.map comment

    [ [ "%! Adobe - PS" ]
    ; [ "" ]
    ; description
    ; [ "" ; comment "## For model:" ]
    ; modelLines
    ; [ ""; comment "## Assumptions:" ]
    ; assumptions
    ; debugBox "Whole page" ({ x = 0M; y = 0M }, measure)
    ] |> Seq.concat



let private chartSpace measure =
    let dim = Math.Min(measure.y / 2M, measure.x)
    let offset = (measure.x - dim) / 2M
    let lower = { x = offset; y = measure.y - dim }
    let higher = { x = lower.x + dim; y = measure.y }
    (lower, higher)


let private chart pageConf model =
    let availableSpace =
        pageMeasure pageConf
            |> chartSpace

    let (center, radius) =
        let min = (fst availableSpace)
        let max = (snd availableSpace)
        let c = (max.x - min.x) / 2M
        ({ x = min.x + c; y = min.y + c }, c * 0.9M)

    let toPiece order item =
        seq {
            yield! [ ""; ""]
            yield sprintf "%d. Chart's piece for Item: '%s'" (order + 1) item.name |> comment
            yield! arcPath (center, radius) (angleFor model item)
            yield setColorFor model item
            yield "fill"
        }

    seq {
        yield! debugBox "Chart" availableSpace
        yield "TODO: Make title" |> comment
        yield! model.items
                |> Seq.mapi toPiece
                |> Seq.concat
    }



let private legendSpace measure =
    let (chartLow, chartHigh) = chartSpace measure
    let lower = { x = chartLow.x; y = 0M }
    let higher = { x = chartHigh.x; y = chartLow.y }
    (lower, higher)


let private legend pageConf model =
    let availableSpace =
        pageMeasure pageConf
            |> legendSpace

    let margin = 4M
    let rowHeight = 20M
    let fontSize = 16M
    let baseLeft = (fst availableSpace).x
    let baseTop = (snd availableSpace).y

    let head =
        seq {
            yield! debugBox "Legend" availableSpace
            yield "/Times-Roman findfont"
            yield sprintf "%s scalefont" (toStr fontSize)
            yield "setfont"
        }

    let toEntry order item =
        let hintSpace =
            let rowOffset = rowHeight * (decimal order)
            ( { x = baseLeft + margin; y = baseTop - rowOffset - rowHeight + margin }
            , { x = baseLeft + (2M * rowHeight) - margin; y = baseTop - rowOffset - margin }
            )

        let nameOrigin =
            let hintLower = fst hintSpace
            let hintHigher = snd hintSpace
            { x = hintHigher.x + (2M * margin); y = hintLower.y }

        seq {
            yield! [ ""; "" ]
            yield sprintf "%d. Legend for Item: '%s'" (order + 1) item.name |> comment
            yield! boxPath hintSpace
            yield setColorFor model item
            yield "fill"
            yield! [ ""; "newpath" ]
            yield cmdSetBlack
            yield sprintf "%s %s moveto" (toStr nameOrigin.x) (toStr nameOrigin.y)
            yield sprintf "(%s : %s) show" (percentage model item) item.name
        }

    seq {
        yield! head
        yield! model.items
                |> Seq.mapi toEntry
                |> Seq.concat
    }



let postScriptOf desc pageConf model =
    let bar =
        comment "=========================================="

    let blockOf what =
        [ ""
        ; bar
        ; sprintf "=== BLOCK : %s" what |> comment
        ; ""
        ] |> List.toSeq

    let blockEnd =
        [ bar
        ; ""
        ] |> List.toSeq

    [ header desc pageConf model
    ; blockEnd
    ; blockOf "Chart's Doughnut"
    ; chart pageConf model
    ; blockEnd
    ; blockOf "Chart's Legend"
    ; legend pageConf model
    ; blockEnd
    ] |> Seq.concat |> Seq.toList
