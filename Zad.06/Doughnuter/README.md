# Doughnuter - PostScript diagram generator in F#

---

## Assumptions:

This program assumes environment has got installed **GSView**: http://artifex.com/developers-ghostscript-gsview/

* Could be acquired via [_Choco_](https://chocolatey.org/):
  * `$ choco install gsview`
* It should be installed under path:
  * `C:\Program Files\Artifex Software\gsview6.0\bin\gsview.exe`
