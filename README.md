# MG Laboratorium - Metody Geometryczne
## PK FMI Stopień 2, Semestr I (studia niestacjonarne), 2017/2018

----

PostScript tutorial: [online](http://www.tailrecursive.org/postscript/postscript.html) / [local copy](postscript-tutorial/index.html)

## A First Guide to PostScript
_Peter Weingartner_ - 24 February, 2006
