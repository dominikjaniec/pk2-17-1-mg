﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

namespace SquarePrinter
{
    public class Program
    {
        private const string GSViewPath = @"C:\Program Files\Artifex Software\gsview6.0\bin\gsview.exe";

        public static void Main(string[] args)
        {
            if (args.Length != 2)
                throw new InvalidOperationException(
                    $"Expecting two arguments: <number of columns> <number of rows>");

            var sizes = new SquareSize(args);
            var script = $@"%! Adobe
{SquareScript(sizes, Kind.Normal)}
{SquareScript(sizes, Kind.Alter)}


0 {PageSize.Height} moveto
{GenerateBoard(sizes)}
";

            Console.WriteLine($"Script:\n{script}");

            var file = Save(script);
            Console.WriteLine($"Running file: {file}");
            Process.Start(GSViewPath, file);
        }

        private static string Save(string script)
        {
            var path = Path.GetTempFileName() + ".ps";
            File.WriteAllText(path, script);
            return path;
        }

        private static string SquareScript(SquareSize size, Kind kind)
        {
            string c = kind == Kind.Alter ? "0.2" : "0.8";
            string a = size.SideStr();

            return $@"
/{SquareName(kind)} {{
    {a} 0 rlineto
    0 {a} rlineto
    -{a} 0 rlineto
    closepath
    {c} setgray
    fill
}} def";
        }

        private static string SquareName(Kind kind)
            => "square" + (kind == Kind.Alter ? "alter" : "normal");

        private static string GenerateBoard(SquareSize size)
            => string.Join("\n\n\n", Enumerable.Range(0, size.M).Select(row => GenerateRow(row, size)));

        private static string GenerateRow(int index, SquareSize size)
            => string.Join("\n", Enumerable.Range(0, size.N).Select(col => GenerateCell(index, col, size)));

        private static string GenerateCell(int rowIndex, int colIndex, SquareSize size)
            => $"{colIndex * size.Side()} {PageSize.Height - (rowIndex * size.Side())} moveto\n"
                + SquareName(ResolveKind(rowIndex, colIndex));

        private static Kind ResolveKind(int rowIndex, int colIndex)
            => rowIndex % 2 == 1
                ? colIndex % 2 == 1 ? Kind.Alter : Kind.Normal
                : colIndex % 2 == 1 ? Kind.Normal : Kind.Alter;

        internal enum Kind
        {
            Normal,
            Alter
        }

        internal static class PageSize
        {
            public static int Width { get; } = 500;
            public static int Height { get; } = 600;
        }

        internal class SquareSize
        {
            public int N { get; }
            public int M { get; }

            public SquareSize(string[] args)
            {
                N = int.Parse(args[0]);
                M = int.Parse(args[1]);
            }

            public decimal Side()
            {
                var xSize = ((decimal)PageSize.Width) / N;
                var ySize = ((decimal)PageSize.Height) / M;
                return Math.Min(xSize, ySize);
            }

            public string SideStr()
                => Side().ToString(CultureInfo.InvariantCulture);
        }
    }
}